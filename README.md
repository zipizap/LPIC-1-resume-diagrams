This repo is a public placeholder to share resume diagrams I use for the LPIC-1.
All diagrams have been created by me except the ones which have an explicit indication of another author or source-url

I use (and recommend) [Inkscape](http://inkscape.org/) to edit the SVG diagrams


All comments/suggestions/improvements are welcome :)
